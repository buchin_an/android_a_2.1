package com.example.home.restore;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    private LinkedList<Editable> input = new LinkedList<>();
    private static final int inputSize = 5;

    private EditText editText;
    private Button enter;
    private Button previous;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        enter = findViewById(R.id.enter);
        previous = findViewById(R.id.previous);
        editText = findViewById(R.id.editText);

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItem();
            }
        });
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                poolItem();
            }
        });

    }


    public void addItem() {
        if (input.size() > inputSize) {
            input.removeFirst();
            input.addLast(editText.getText());
            editText.setText("");
        } else {
            input.addLast(editText.getText());
            editText.setText("");
        }
    }

    public void poolItem() {
        editText.setText(input.pollLast());
    }

}
